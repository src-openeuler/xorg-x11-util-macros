%global packagename util-macros
%global debug_package %{nil}

Name: 		xorg-x11-util-macros
Version: 	1.20.0
Release: 	1
License: 	MIT
URL: 		http://www.x.org
BuildArch: 	noarch
Summary: 	X.Org X11 macros
Source0:  	https://www.x.org/archive//individual/util/util-macros-%{version}.tar.gz

Requires: autoconf automake libtool pkgconfig

%description
X.Org X11 autotools macros required for building the various packages that
comprise the X Window System.

%prep
%setup -q -n %{packagename}-%{version}

%build
%configure
%make_build

%install
%make_install

%files
%doc COPYING ChangeLog
%{_datadir}/aclocal/xorg-macros.m4
%{_datadir}/pkgconfig/xorg-macros.pc
%{_datadir}/util-macros

%changelog
* Thu Jun 08 2023 wulei <wu_lei@hoperun.com> - 1.20.0-1
- Update to 1.20.0

* Fri Jan 14 2022 wangkai <wangkai385@huawei.com> - 1.19.3-1
- Update to 1.19.3

* Wed Nov 27 2019 hexiaowen <hexiaowen@huawei.com> - 1.19.2-4
- Rebuilt

* Wed Nov 27 2019 hexiaowen <hexiaowen@huawei.com> - 1.19.2-3
- Rebuilt

* Tue Aug 27 2019 hexiaowen <hexiaowen@huawei.com> - 1.19.2-2
- Renew HTTPS URLs

* Tue Aug 27 2019 hexiaowen <hexiaowen@huawei.com> - 1.19.2-1
- util-macros 1.19.2
